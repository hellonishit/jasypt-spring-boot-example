package com.spring.jasypt.encryption.encryption;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class EncryptPropertyLogBean {
	private static final Logger logger = LoggerFactory
			.getLogger(EncryptPropertyLogBean.class);
	@Value("${encrypted.property}")
	private String encryptedProp;

	@Value("${nonencrypted.property}")
	private String nonEncryptedProp;
	
	@Autowired
	private Environment env;

	@PostConstruct
	public void postCall() {
		logger.info("-------------------------------");
		logger.info("\nencrypted.property = " + encryptedProp);
		logger.info("\nnonencrypted.property = " + nonEncryptedProp);
		logger.info("\nencrypted.property = " + env.getProperty("encrypted.property") + " using Environment");
		logger.info("\nnonencrypted.property = " + env.getProperty("nonencrypted.property") + " using Environment");
		logger.info("-------------------------------");
	}
}
