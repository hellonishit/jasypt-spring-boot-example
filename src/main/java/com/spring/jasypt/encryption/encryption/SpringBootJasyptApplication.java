package com.spring.jasypt.encryption.encryption;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootJasyptApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJasyptApplication.class, args);
	}

}
