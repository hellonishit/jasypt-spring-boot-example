## Example of jasypt and spring boot

mvn -Djasypt.encryptor.algorithm=PBEWithMD5AndDES -Djasypt.encryptor.password=secretkey spring-boot:run


## Generate encrypted properties 
java -cp jasypt-1.9.2.jar org.jasypt.intf.cli.JasyptPBEStringEncryptionCLI input="Example of jasypt and spring boot" password=secretkey algorithm=PBEWithMD5AndDES